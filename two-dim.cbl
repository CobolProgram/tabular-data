       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  TWO-DIME-TABLE.
       AUTHOR.   MMODPOWW.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  WS-A-VALUE.
           05 FILLER   PIC X(3)   VALUE "111".
           05 FILLER   PIC X(3)   VALUE "222".
           05 FILLER   PIC X(3)   VALUE "333".
           05 FILLER   PIC X(3)   VALUE "444".
           05 FILLER   PIC X(3)   VALUE "555".
       01  WS-TABLE REDEFINES WS-A-VALUE.
           05 WS-ROW   OCCURS   3  TIMES.
              10 WS-COL   PIC  X OCCURS 3 TIMES VALUE "-".
       01  WS-IDX-ROW  PIC   9.
       01  WS-IDX-COL  PIC   9.

       PROCEDURE DIVISION.

       BEGIN.
           DISPLAY WS-TABLE
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              PERFORM VARYING WS-IDX-COL FROM 1 BY 1
                 UNTIL WS-IDX-COL > 3
                    DISPLAY WS-COL (WS-IDX-ROW, WS-IDX-COL)
              END-PERFORM
           END-PERFORM
       .
